
def upper_lower():
	text = input('Введите текст: ')       
    up = sum(1 for i in text if i.isupper())
    low = sum(1 for i in text if i.islower())

    print('В тексте {} букв верхнем регистре, {} в нижнем регистре.').format(up%100, low%100)

upper_lower()