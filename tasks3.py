
num = input().split()
l_num = list(map(int, num))
s = max(l_num)
e = l_num()
num_s = sorted(range(s, e+1))
result = []

for i in num_s:
  if i is not l_num:
    result.append(i) 

if result != l_num:
  a = sum(result) - sum(l_num)
  print(a)
elif l_num < 0:
  print(1)